/*
Titulo: Imagem
Tipo:View
FUncao: Tipo usado carregar e manipular as imagens exibidas, tanto de entidades quanto do mapa do jogo.
*/

#include "imagem.h"
#include "render.h"

Imagem::Imagem()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

void Imagem::free()
{
	//Free texture if it exists
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

bool Imagem::loadFromFile( std::string path, bool useColorKeying)
{
	//Get rid of preexisting texture
	free();

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Color key image
		if(useColorKeying)
			SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

		//Create texture from surface pixels
        	newTexture = SDL_CreateTextureFromSurface( Render::gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}
		else
		{
			//Get image dimensions
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}

void Imagem::render( int x, int y )
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };
	SDL_RenderCopy( Render::gRenderer, mTexture, NULL, &renderQuad );
}

void Imagem::viewPortRender(SDL_Rect playerViewPort)
{
	//Set rendering space and render to screen
	SDL_RenderSetViewport( Render::gRenderer, &playerViewPort ); 
	SDL_RenderCopy( Render::gRenderer, mTexture, NULL, NULL);
}

int Imagem::getWidth(){
	return this->mWidth;
}

int Imagem::getHeight(){
	return this->mHeight;
}