FSERVER = mainServer.cpp render.cpp imagem.cpp entidade.cpp entidades.cpp som.cpp colisor.cpp controle.cpp heroi.cpp monstro.cpp AABBTree.cpp AABBNode.cpp mapa.cpp zumbi.cpp servidor.cpp

FCLIENT = mainClient.cpp render.cpp imagem.cpp entidade.cpp entidades.cpp mapa.cpp colisor.cpp AABBNode.cpp AABBTree.cpp som.cpp

FCOMUM = render.cpp imagem.cpp entidade.cpp entidades.cpp heroi.cpp som.cpp mapa.cpp monstro.cpp zumbi.cpp AABBTree.cpp AABBNode.cpp colisor.cpp jsonManager.cpp

FSERVER = mainServer.cpp controle.cpp servidor.cpp $(FCOMUM)

FCLIENT = mainClient.cpp $(FCOMUM)

all: server client

server: $(FSERVER)
	g++ -std=c++11 -Wall $(FSERVER)  -I./SDL2 -lSDL2 -lSDL2_image -lSDL2_mixer -pthread -o server


client: $(FCLIENT)
	g++ -std=c++11 $(FCLIENT) -I./SDL2 -lSDL2 -lSDL2_image -lSDL2_mixer -o client

AABBNode.so: AABBNode.cpp AABBNode.h
	g++ -fPIC -c AABBNode.cpp
	g++ -shared -o AABBNode.so AABBNode.o

AABBTree.so: AABBTree.cpp AABBTree.h AABBNode.so
	g++ -fPIC -c AABBTree.cpp -L./ -lAABBNode
	g++ -shared -o AABBTree.so AABBTree.o

clean:
	rm -f client server

run: all
	./server &
	sleep 3s
	./client 127.0.0.1 &





