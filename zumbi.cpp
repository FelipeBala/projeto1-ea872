/*
Titulo: ZUmbi
Tipo: MOdel
FUncao: IMplementa tipo especifico de monstro
*/

#include "zumbi.h"


Zumbi::Zumbi(int px, int py, int level) : Monstro(px, py, "imagens/zumbi", level){}


Zumbi::Zumbi(int px, int py) : Monstro(px, py, "imagens/zumbi",1){}


