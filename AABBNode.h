#ifndef EA872AABBNode
#define EA872AABBNode

class AABBNode{
	public:
		int minX, maxX, minY, maxY;
		int height;
		AABBNode *left, *right, *parent;
		void *entity;
		AABBNode();
		AABBNode(void *_entity, int _minX, int _maxX,  int _minY,  int _maxY);
		AABBNode(AABBNode *n1, AABBNode *n2);
		bool isLeaf();
		bool isRoot();
		int increasedArea(AABBNode *inserted);
		void mergeBoxes(AABBNode *inserted);
		bool collide(AABBNode *other);
		int balance();
		void adjustBox(AABBNode *leftChild, AABBNode *rightChild);
};

#endif
