#include "servidor.h"
#include "controle.h"
#include "jsonManager.h"

#include <stropts.h>
#include <poll.h>

#define TempoEspera 1000

char Servidor::comando='t';

std::mutex Servidor::mtx;


void Servidor::configuraServidor(unsigned int porta){
  int socket_fd, connection_fd;
  struct sockaddr_in myself;


  socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  printf("Socket criado\n");

  myself.sin_family = AF_INET;
  myself.sin_port = htons(porta);
  inet_aton("0.0.0.0", &(myself.sin_addr));

  printf("Tentando abrir porta %u \n", porta);
  if (bind(socket_fd, (struct sockaddr*)&myself, sizeof(myself)) != 0) {
    printf("Problemas ao abrir porta\n");
    close(socket_fd);
    return;
  }
  printf("Abri porta %u!\n", porta);

  listen(socket_fd, 14);
  printf("Estou ouvindo na porta %u!\n", porta);

  char ip_client[INET_ADDRSTRLEN];
  while(true){
          struct sockaddr_in client;
          socklen_t client_size = (socklen_t)sizeof(client);
	  printf("Vou travar ate receber algum cliente\n");
	  connection_fd = accept(socket_fd, (struct sockaddr*)&client, &client_size);
          /* Identificando cliente */
          inet_ntop( AF_INET, &(client.sin_addr), ip_client, INET_ADDRSTRLEN );
          printf("Novo cliente com IP: %s\n", ip_client);
	  std::thread t(Servidor::novoCliente, connection_fd);
          t.detach();
  }

  close(socket_fd);
}


void Servidor::novoCliente(int connection_fd){

  char input_buffer[buffer];
  char output_buffer[buffer];

  int rv;
  struct pollfd ufds;
  //Cria regras do poll de I/O.
  ufds.fd = connection_fd;
  ufds.events = POLLIN | POLLPRI;  
  
  srand (time(NULL));
  Heroi *player = Controle::criaHeroi("imagens/character");
  //Heroi::ListaJogadores.push_back(player);

  printf("novoCliente\n");


  while (true) {

    rv = poll(&ufds, 1, TempoEspera);
    if(rv > 0 && (ufds.revents & POLLIN || ufds.revents & POLLPRI)){
        //servidor recebe o Json do cliente
        if (recv(connection_fd, input_buffer, buffer, 0) > 0){
            Servidor::consomeJson(input_buffer, player);
        }
        else{
          delete(player);
          break;
        }
    }

    //printf("%s\n", input_buffer);

    Servidor::criaJson(*player, output_buffer);

    //strcpy(output_buffer,"PONG");
    if (send(connection_fd, output_buffer, strlen(output_buffer), 0) < 0) {
      printf("Erro ao enviar mensagem para o cliente\n");
    }

  }

  return;

}


void Servidor::consomeJson( char *input_buffer, Heroi *player){
//Me'todo que copia os dados transmitidos presentes no Json para as estruturas de dados do servidor. 
//cliente altera somente os dados referentes aquele cliente.

  //Servidor::mtx.lock();
   //usa lock para somente um cliente alterar os dados por vez e evitar corridas aos dados.
   const std::string s(input_buffer);
   std::string comando;
   JsonManager::leJsonComando(comando, s);

   if(comando[0] == 'w'){ //Render::tecla == 'w'	
	Controle::move(CIMA,player);	
	}
   else if(comando[0] == 's'){	
	Controle::move(BAIXO,player);
	}
   else if(comando[0] == 'd'){	
	Controle::move(DIREITA,player);
   }
   else if(comando[0] == 'a'){	
	Controle::move(ESQUERDA,player);
   }
   else if(comando[0] == 'j'){
	Controle::ataca(player);
	}

  //Servidor::mtx.unlock();

}


void Servidor::criaJson(Heroi player, char *ouput_buffer){
//Transforma o estado interno do servidor para a string Json

std::string s("");
JsonManager::criaJsonMundo(player, s);
strcpy(ouput_buffer,s.c_str());
//printf("\nCriaJson: %s", ouput_buffer);

}








