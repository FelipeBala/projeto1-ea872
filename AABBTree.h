#ifndef EA872AABBTree
#define EA872AABBTree

#include "AABBNode.h"
#include <vector>

class AABBTree{
	private:
		AABBNode *root;
	public:
		AABBTree();
		void insert(AABBNode *newNode);
		std::vector<void*> query(AABBNode *newNode);
		AABBNode* balance(AABBNode *parentNode);
		AABBNode* rotateLeft(AABBNode *root);
		AABBNode* rotateRight(AABBNode *root);
		void remove(AABBNode *removedNode);
};

#endif
