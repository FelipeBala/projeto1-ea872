/*
Titulo: Render
Tipo: View
FUncao: Renderiza na tela as imagens do mapa e entidades, alem de receber eventos gerados por usuarios pressionando teclas.
OBS: Grande parte dessa biblioteca foi retirada do Lazy FOO's guide de SDL 2.0.
*/

#ifndef EA872render
#define EA872render
#include "universalHeader.h"
#include "imagem.h"
#include "entidades.h"
#include "mapa.h"
#include "heroi.h"


class Render
{
	public:
		static bool init(int SCREEN_WIDTH, int SCREEN_HEIGHT);
		static bool DestroyWindow();

		static void renderWorld(Entidade *jogador, Mapa mapaGeral);

		static void HandleEvent();
		
		static char tecla;
		//Quit the game
		static bool quit;

		//The window we'll be rendering to
		static SDL_Window* gWindow;

		//The window renderer
		static SDL_Renderer* gRenderer;
		
		//Event handler
		static SDL_Event event;

		//Dimensoes da Tela
		static int SCREEN_WIDTH;
		static int SCREEN_HEIGHT;
};


#endif
