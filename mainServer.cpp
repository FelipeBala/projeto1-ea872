#include "universalHeader.h"
#include "render.h"
#include "imagem.h"
#include "entidades.h"
#include "som.h"
#include "mapa.h"
#include "controle.h"
#include "heroi.h"
#include "monstro.h"
#include "zumbi.h"
#include "servidor.h"

#include <thread>


int main(){

	//inicializa renderizador e tela 640x480 pixels
	Render::init(640,480);

	//Monstros e jogadores inciais
	Heroi *jogador = new Heroi(100,100,"imagens/character");
	new Zumbi(300,300);
	new Monstro(400,400,"imagens/balrog", 1);

	//Mapa do jogo
	Mapa mapaGeral;

	mapaGeral.carregarImagem("imagens/mapa.png");

	//Carrega sons do jogo
	Som::iniciaPlayer();
	Som a("som/scratch.wav",1);
	Som::adiciona("som/high.wav",2);
	Som::adiciona("som/step.wav",3);

	std::thread t1(Servidor::configuraServidor, 3001);
        t1.detach();

	//Ateh fechar a janela
	while(!Render::quit){
		//Renderiza mapa do jogo (view)
		Render::renderWorld(jogador, mapaGeral);

		Servidor::mtx.lock();
		

		//Servidor::comando = '0';
		//Trata entradas do usuario
//Render::tecla == 'w'	
//Servidor::comando

		if(Render::tecla == 'w'){	
			Som::adiciona("som/step.wav",3);
			Controle::move(CIMA,jogador);
			
		}
		else if(Render::tecla == 's'){	
			Som::adiciona("som/step.wav",3);
			Controle::move(BAIXO,jogador);
		}
		else if(Render::tecla == 'd'){	
			Som::adiciona("som/step.wav",3);
			Controle::move(DIREITA,jogador);
		}
		else if(Render::tecla == 'a'){	
			Som::adiciona("som/step.wav",3);
			Controle::move(ESQUERDA,jogador);
		}
		else if(Render::tecla == 'j'){
			Som::toca(1);
			Controle::ataca(jogador);
		}
		else if(Render::tecla == 'l'){
			printf("lista:\n");
			for(auto ent: Entidades::listaEntidades){
				printf("(%ld : %s )", ent->id, ent->tipo == HEROI? "Heroi":"Monstro");
			}
			printf("\n");
		}

		//Controle do jogo:
		//Possivelmente spawna novos monstros
		Controle::criaMonstro();
		//Movimenta mosntros em direcao aos jogadores
		Controle::movimentaMonstros();
		//Da a oportunidade dos monstros atacarem
		Controle::monstrosAtacam();
		//Da a oportunidade do controlador restaurar o estado de monstros e jogadores de atacando para andando.
		Controle::restauraEstados();
		Servidor::mtx.unlock();
	}
	//Fecha janela
	Render::DestroyWindow();


	return 0;

}
