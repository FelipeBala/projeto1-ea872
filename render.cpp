/*
Titulo: Render
Tipo: View
FUncao: Renderiza na tela as imagens do mapa e entidades, alem de receber eventos gerados por usuarios pressionando teclas.
OBS: Grande parte dessa biblioteca foi retirada do Lazy FOO's guide de SDL 2.0.
*/

#include "render.h"

int Render::SCREEN_WIDTH = 0;
int Render::SCREEN_HEIGHT = 0;
SDL_Window* Render::gWindow = NULL;
SDL_Renderer* Render::gRenderer = NULL;
bool Render::quit = false;
SDL_Event Render::event;
char Render::tecla=' ';

//Destroi janela existente
bool Render::DestroyWindow(){
	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();

	return true;
}

//Incializa nova janela com dimensoes dadas
bool Render::init(int WIDTH, int HEIGHT)
{
	//Initialization flag
	bool success = true;
	Render::SCREEN_WIDTH = WIDTH;
	Render::SCREEN_HEIGHT = HEIGHT;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "EA872 - Projeto 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

//Imprime na tela entidades e parte relevante do mapa
void Render::renderWorld(Entidade *jogador, Mapa mapaGeral){


	//Clear screen
	SDL_SetRenderDrawColor( Render::gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
	SDL_RenderClear( Render::gRenderer );

	SDL_Rect playerViewport;
	playerViewport.x = -jogador->x - SCREEN_HEIGHT/2; 
	playerViewport.y = -jogador->y - SCREEN_WIDTH/2; 
	playerViewport.w = 4*SCREEN_WIDTH; 
	playerViewport.h = 4*SCREEN_HEIGHT; 
	mapaGeral.renderiza(playerViewport);

	jogador->renderiza((4*SCREEN_WIDTH)/5, SCREEN_HEIGHT);

	//Render Foo' to the screen
  	for (std::set<Entidade*>::iterator it = Entidades::listaEntidades.begin() ; it != Entidades::listaEntidades.end(); ++it){
		if((*it)->rederizavel && (*(*it)).id != jogador->id){
			(*it)->renderiza((4*SCREEN_WIDTH)/5, SCREEN_HEIGHT);
		}

	}
    	

	//Update screen
	SDL_RenderPresent( Render::gRenderer );

	Render::HandleEvent();

}

//Trata eventos gerados por usuarios usando teclas
void Render::HandleEvent(){
	//Handle events on queue
	Render::tecla=' ';
	while( SDL_PollEvent( &Render::event ) != 0 )
	{
		//User requests quit
		if( Render::event.type == SDL_QUIT )
		{
			Render::quit = true;
		}
		else if( Render::event.type == SDL_KEYDOWN ) { 
			//Select key pressed
			switch( Render::event.key.keysym.sym ) {
 				case SDLK_w: Render::tecla='w';break;
				case SDLK_s: Render::tecla='s';break;
				case SDLK_a: Render::tecla='a';break; 
				case SDLK_d: Render::tecla='d';break;
				case SDLK_j: Render::tecla='j';break; 
				case SDLK_l: Render::tecla='l';break; 
				default: Render::tecla=' ';break; 
			}
		}

	}

}


