/*
Titulo: Colisor
Tipo:COntrol
FUncao: Usado para detectar colisoes entre as caixas de cada entidade (heroi e monstros). Usa AABB Tree para ganho de eficiencia
*/

#ifndef EA872Colisor
#define EA872Colisor

#include "universalHeader.h"
#include "AABBTree.h"
#include <map>
#include "entidades.h"

class Colisor
{
	public:
		static AABBTree *arvore;
		static std::map<Entidade*, AABBNode *> mapaEntidadeNo;

	public:
		Colisor();
		static void insere(Entidade *entidadeInserida);
		static bool remove(Entidade *entidadeRemovida);
		static std::vector<void*> checaColisao(Entidade *entidadeConsultada);
		static std::vector<void*> checaColisao(AABBNode *noConsultado);
};

#endif
