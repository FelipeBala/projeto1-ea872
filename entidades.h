/*
Titulo: Entidades
Tipo:Model
FUncao: Tipo usado para manter o conjunto de todas as entidades existentes no mapa e iterar por elas quano necessario.
*/

#ifndef EA872entidades
#define EA872entidades

#include "universalHeader.h"
#include <set>
#include <map>
#include "entidade.h"

class Entidades
{
	public:
		static void addEntidade(Entidade*);
		static bool removeEntidade(Entidade*);
		static void esvaziaEntidades();
		static std::set<Entidade*> listaEntidades;
		static bool removeAllEnt();
		static void addMap(std::map<int, Entidade> mapaIdEntidade);
		static bool removeEntidadeSemDeletar(Entidade* e);
		
};


#endif
