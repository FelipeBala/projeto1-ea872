#ifndef EA872anotherPlayer
#define EA872anotherPlayer

#include "universalHeader.h"
#include "entidade.h"
#include "monstro.h"



class AnotherPlayer : public Entidade
{
	public:
		AnotherPlayer(int px, int py, std::string path, int level)
		int vidaMaxima();

};


#endif

AnotherPlayer::AnotherPlayer(int px, int py, std::string path, int level)
	:Entidade(px, py, path){
		this->level = 1;
		this->recompensaExp = 0;
		this->vida = this->vidaMaxima();
		this->tipo = HEROI;
	}


//Vida maxima, depndente de level
int AnotherPlayer::vidaMaxima(){
	return 80 + 20*this->level;
}
