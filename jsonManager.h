/*
Titulo: Render
Tipo: View
FUncao: Renderiza na tela as imagens do mapa e entidades, alem de receber eventos gerados por usuarios pressionando teclas.
OBS: Grande parte dessa biblioteca foi retirada do Lazy FOO's guide de SDL 2.0.
*/

#ifndef EA872jsonMannager
#define EA872jsonMannager
#include "universalHeader.h"
#include "entidades.h"
#include "mapa.h"
#include "heroi.h"
#include <string>
#include <map>


class JsonManager
{
	public:
		static void criaJsonMundo(Heroi h, std::string &s);
		static bool leJsonMundo(Heroi &h, const std::string json);
		static void criaJsonComando(std::string comando, std::string &s);
		static bool leJsonComando(std::string &comando, const std::string json);
};


#endif
