/*
Titulo: Render
Tipo: View
FUncao: Renderiza na tela as imagens do mapa e entidades, alem de receber eventos gerados por usuarios pressionando teclas.
OBS: Grande parte dessa biblioteca foi retirada do Lazy FOO's guide de SDL 2.0.
*/

#include "universalHeader.h"
#include "entidades.h"
#include "mapa.h"
#include "heroi.h"
#include <string>
#include <map>
#include <iostream>
#include "jsonManager.h"

bool buscaObjetoJson(std::string target, std::string json, int &ind);
bool buscaIntJson(std::string target, std::string json, int &res, int &ind);
std::vector<int> kmpPre(std::string P);
bool kmpSearch(std::string pattern, std::string text, int &ini);
bool buscaStringJson(std::string target, std::string json, std::string &res, int &ind);
bool buscaProxLista(const std::string json, int &ind);


/*
* Funcao: criaJson
* Descricao: Cria uma string no fomato Json com as entidades relevantes para a criação da tela
*  Este consiste de dois campos de valor, um com um objeto contendo os dados do heroi, e outro 
*  com uma lista de objetos, cada um representando uma entidade (diferente do heroi passado) no mapa. 
* Entradas:
* * h: Heroi do usuario que recebera o JSON. Deve ser diferenciado pois a tela eh centrada nele
* * s: String que recebe o objeto json criado. 
*/
void JsonManager::criaJsonMundo(Heroi h, std::string &s){
	s.clear();
	s += "{";

	//Adiciona o heroi na string
	s += "\"heroi\":{";
	s += "\"id\":" + std::to_string(h.id)  + ",";
	s += "\"x\":" + std::to_string(h.x)  + ",";
	s += "\"y\":" + std::to_string(h.y) + ",";
	s += "\"orientacao\":" + std::to_string(h.orientacao) + ",";
	s += "\"acao\":" + std::to_string(h.acao) + ",";
	s += "\"imagePath\":\"" + h.imagePath + "\"" ;
	s += "}";

	s += ',';

	//Adiciona strings para entidades renderizaveis diferentes do heroi
	bool flag_first = true;
	s += "\"entidades\":[";
	for(auto ent: Entidades::listaEntidades){
		if(ent->rederizavel && ent->id != h.id){
			if(flag_first)
				flag_first = false;
			else
				s += ",";
			s += "{";

			s += "\"id\":" + std::to_string(ent->id)  + ",";
			s += "\"x\":" + std::to_string(ent->x)  + ",";
			s += "\"y\":" + std::to_string(ent->y) + ",";
			s += "\"orientacao\":" + std::to_string(ent->orientacao) + ",";
			s += "\"acao\":" + std::to_string(ent->acao) + ",";
			s += "\"imagePath\":\"" + ent->imagePath + "\"";

			s += "}";
		}
	}
	s += "]";

	s += "}";
}

void JsonManager::criaJsonComando(std::string comando, std::string &s){
	s.clear();
	s += "{";
	s += "\"comando\":\"" + comando + "\"";
	s += "}";
}

bool JsonManager::leJsonMundo(Heroi &h, const std::string json){
	int ind = 0;
	int auxI;
	std::string auxS;

	//Le campo do heroi
	if(!buscaObjetoJson("heroi", json, ind))
		return 0;

	if(!buscaIntJson("id", json, auxI, ind))
		return 0;
	h.id = auxI;

	if(!buscaIntJson("x", json, auxI, ind)){
		return 0;
	}
	h.x = auxI;

	if(!buscaIntJson("y", json, auxI, ind))
		return 0;
	h.y = auxI;

	if(!buscaIntJson("orientacao", json, auxI, ind))
		return 0;
	h.orientacao = (orientacaoHorizontal)auxI;

	if(!buscaIntJson("acao", json, auxI, ind))
		return 0;
	h.acao = (tipoAcao)auxI;

	if(!buscaStringJson("imagePath", json, auxS, ind))
		return 0;
	h.imagePath = auxS;

	//Le entidades
	if(!buscaObjetoJson("entidades", json, ind))
		return 0;

	//Mapa de ids para entidades
	std::map<int, Entidade> mapaIdEntidade;

	Entidades::esvaziaEntidades();

	while(buscaProxLista(json, ind)){
		Entidade *ent = new Entidade();

		if(!buscaIntJson("id", json, auxI, ind))
			return 0;
		ent->id = auxI;

		if(!buscaIntJson("x", json, auxI, ind))
			return 0;
		ent->x = auxI;

		if(!buscaIntJson("y", json, auxI, ind))
			return 0;
		ent->y = auxI;

		if(!buscaIntJson("orientacao", json, auxI, ind))
			return 0;
		ent->orientacao = (orientacaoHorizontal)auxI;

		if(!buscaIntJson("acao", json, auxI, ind))
			return 0;
		ent->acao = (tipoAcao)auxI;

		if(!buscaStringJson("imagePath", json, auxS, ind))
			return 0;
		ent->imagePath = auxS;
		ent->carregarImagem(auxS);
	}

	return true;
}

bool buscaProxLista(const std::string json, int &ind){
	while(ind < (int)json.size()){
		if(json[ind] == ',' || json[ind] == '[')
			return true;
		if(json[ind] == ']')
			return false;
		ind++;
	}

	return false;
}

bool JsonManager::leJsonComando(std::string &comando, const std::string json){
	int ind = 0;
	comando.clear();
	return buscaStringJson("comando", json, comando, ind);
}


bool buscaObjetoJson(std::string target, std::string json, int &ind){
	return kmpSearch("\"" + target + "\":", json, ind);
}

bool buscaIntJson(std::string target, std::string json, int &res, int &ind){

	if(!kmpSearch("\"" + target + "\":", json, ind))
		return false;

	res = 0;
	bool atLeastOne = false;
	bool negative = false;
	if(json[ind] == '-'){
		negative = true;
		ind++;
	}
	while(json[ind] >= '0' && json[ind] <= '9'){
		res *= 10;
		res += json[ind++] - '0';
		atLeastOne = true;
	}

	if(negative)
		res *= -1;

	return atLeastOne;
}

bool buscaStringJson(std::string target, std::string json, std::string &res, int &ind){

	if(!kmpSearch("\"" + target + "\":\"", json, ind))
		return false;
	
	res = "";

	while(json[ind] != '\"' && ind < (int)json.size()){
		res += json[ind++];
	}

	if(ind == (int)json.size() || res.size() == 0)
		return false;

	return true;
}


std::vector<int> kmpPre(std::string P){
	int i = 0, j = -1;
	int m = P.size();
	std::vector<int> b(m+1);
	b[0] = -1;

	while( i < m)
	{
		while(j >= 0 && P[i] != P[j]) 
			j = b[j];
		i++, j++;
		b[i] = j;
	}

	return b;
}

bool kmpSearch(std::string pattern, std::string text, int &ini){
	int m = pattern.size();
	int i = ini, j = 0, n = text.size();
	std::vector<int> returnVec = kmpPre(pattern);
	do{
		while(j >= 0 && text[i] != pattern[j]) 
			j = returnVec[j];
		i = (i + 1)%n;
		j++;

		if(j == m)
		{
			ini = i;
			return true;
		}
	}while(i != ini);

	return false;
}
