/*
*Modulo: AABBTree
*Tipo: Control
*Funcao: Usada para monitorar colisoes de forma eficiente. Implementacao propria baseada no seguinte tutorial:
https://www.azurefromthetrenches.com/introductory-guide-to-aabb-tree-collision-detection/
*/

#include "AABBTree.h"
#include <stddef.h>
#include <stack>
#include <cstdlib>

enum chosenSide{
	LEFT, RIGHT
};

AABBTree::AABBTree(){
	this->root = NULL;
} 

void AABBTree::insert(AABBNode *newNode){
	if(this->root == NULL){
		root = newNode;
		return;
	}

	std::stack<AABBNode*> visitedNodes;
	std::stack<chosenSide> chosenChild;

	AABBNode *currentNode = this->root;

	while(currentNode->isLeaf() == false){
		visitedNodes.push(currentNode);

		currentNode->mergeBoxes(newNode);

		(currentNode->left)->increasedArea(newNode);

		(currentNode->right)->increasedArea(newNode);

		if((currentNode->left)->increasedArea(newNode) > (currentNode->right)->increasedArea(newNode)){
			currentNode = currentNode->right;
			chosenChild.push(RIGHT);
		}

		else{
			currentNode = currentNode->left;
			chosenChild.push(LEFT);
		}
	}


	AABBNode *newBranchFather = currentNode->parent;

	AABBNode *newBranch = new AABBNode(currentNode, newNode);

	newBranch->parent = newBranchFather;

	currentNode = newBranch;

	AABBNode *parentNode;

	while(!visitedNodes.empty()){
		parentNode = visitedNodes.top();
		visitedNodes.pop();

		if(chosenChild.top() == LEFT){
			parentNode->left = currentNode;
		}
		
		else{
			parentNode->right = currentNode;
		}

		parentNode = this->balance(parentNode);

		chosenChild.pop();

		currentNode = parentNode;
	}

	this->root = currentNode;
}

std::vector<void*> AABBTree::query(AABBNode *qNode){
	std::vector<void*> collisions;

	if(this->root != NULL){
		std::stack<AABBNode*> possibleCollisions;

		possibleCollisions.push(this->root);

		AABBNode *currentNode;

		while(!possibleCollisions.empty()){
			currentNode = possibleCollisions.top();
			possibleCollisions.pop();

			if(currentNode->collide(qNode)){
				if(currentNode->isLeaf())
					collisions.push_back(currentNode->entity);
				else{
					possibleCollisions.push(currentNode->left);
					possibleCollisions.push(currentNode->right);
				}
			}
		}
	}

	return collisions;
}

AABBNode* AABBTree::balance(AABBNode *parentNode){
	if(parentNode->balance() > 1){
		if(parentNode->right->balance() >= 1){
			parentNode = this->rotateLeft(parentNode);
		}

		else{
			this->rotateRight(parentNode->right);
			parentNode = this->rotateLeft(parentNode);
		}
	}

	else if(parentNode->balance() < -1){
		if(parentNode->left->balance() <= -1){
			parentNode = this->rotateRight(parentNode);
		}

		else{
			this->rotateLeft(parentNode->left);
			parentNode = this->rotateRight(parentNode);
		}
	}
		
	return parentNode;
}

AABBNode* AABBTree::rotateLeft(AABBNode *root){
	AABBNode *newRoot = root->right;

	root->right = newRoot->left;
	newRoot->left = root;
	newRoot->parent = root->parent;

	*root = AABBNode(root->left, root->right);
	*newRoot = AABBNode(newRoot->left, root->right); 

	return newRoot;
}

AABBNode* AABBTree::rotateRight(AABBNode *root){
	AABBNode *newRoot = root->left;

	root->left = newRoot->right;
	newRoot->right = root;
	newRoot->parent = root->parent;

	*root = AABBNode(root->left, root->right);
	*newRoot = AABBNode(newRoot->left, root->right); 

	return newRoot;
}

void AABBTree::remove(AABBNode *removedNode){
	if(!removedNode->isLeaf())
		return;

	if(removedNode->parent != NULL){
		AABBNode *father = removedNode->parent, *grandFather, *sibling;

		if(father->left == removedNode)
			sibling = father->right;
		else
			sibling = father->left;

		sibling->parent = grandFather = father->parent;

		if(grandFather != NULL){
			if(grandFather->right == father)
				grandFather->right = sibling;
			else
				grandFather->left = sibling;
		}

		else
			this->root = sibling;

		delete father;
		delete removedNode;

		AABBNode *currentNode = sibling;

		while(currentNode != NULL){
			if(std::abs(currentNode->balance()) > 1)
				this->balance(currentNode);
			else if(!currentNode->isLeaf()){
				currentNode->adjustBox(currentNode->left, currentNode->right);
				currentNode->height = std::max(currentNode->left->height, currentNode->right->height);
			}

			currentNode = currentNode->parent;
		}

	}

	else{
		delete removedNode;
		this->root = NULL;
	}
}