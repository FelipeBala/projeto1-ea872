/*
*Modulo: Heroi
*Tipo: Model
*Funcao: Implementa caracteristicas do heroi que permitem que ele atue (ande, entre em combate, etc)
*/
#include "universalHeader.h"
#include "heroi.h"
#include "monstro.h"
#include "entidades.h"
#include "colisor.h"

std::vector<Heroi> Heroi::ListaJogadores;

//Heroi monstro em posicao (px py), carrega imagem de path
Heroi::Heroi(int px, int py, std::string path)
	:Entidade(px, py, path){
		this->level = 1;
		this->exp = 0;
		this->vida = this->vidaMaxima();
		this->tipo = HEROI;
	}


//Vida maxima, depndente de level
int Heroi::vidaMaxima(){
	return 80 + 20*this->level;
}

//Exp para upar para o proximo level (level atual vezes sua raiz)
int Heroi::expProximoLevel(){
	return this->level*sqrt(this->level);
}

//Modificador de ataque dependente do level
int Heroi::modificadorAtaque(){
	return 2*level;
}


//Modificador de defesa dependente do level
int Heroi::modificadorDefesa(){
	return level;
}

