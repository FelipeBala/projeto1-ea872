/*
Titulo: Colisor
Tipo:COntrol
FUncao: Usado para detectar colisoes entre as caixas de cada entidade (heroi e monstros). Usa AABB Tree para ganho de eficiencia
*/

#include "colisor.h"

AABBTree* Colisor::arvore = new AABBTree();
std::map<Entidade*, AABBNode*> Colisor::mapaEntidadeNo;


void Colisor::insere(Entidade *entidadeInserida){
	AABBNode *noRepresentante = new AABBNode(entidadeInserida, 
		entidadeInserida->x, entidadeInserida->x + entidadeInserida->getWidth(), entidadeInserida->y, 
			entidadeInserida->y + entidadeInserida->getHeight());

	mapaEntidadeNo[entidadeInserida] = noRepresentante;

	arvore->insert(noRepresentante);
}

bool Colisor::remove(Entidade *entidadeRemovida){
	std::map<Entidade*, AABBNode*>::iterator it = mapaEntidadeNo.find(entidadeRemovida);
	if(it != mapaEntidadeNo.end()){
		arvore->remove((*it).second);
		mapaEntidadeNo.erase(it);
		return true;
	}

	return false;
}

std::vector<void*> Colisor::checaColisao(Entidade *entidadeConsultada){
	AABBNode *noRepresentante = new AABBNode(entidadeConsultada, 
		entidadeConsultada->x, entidadeConsultada->x + entidadeConsultada->getWidth(), 
			entidadeConsultada->y, entidadeConsultada->y + entidadeConsultada->getHeight());

	return arvore->query(noRepresentante);
}

std::vector<void*> Colisor::checaColisao(AABBNode *noConsultado){
	return arvore->query(noConsultado);
}