/*
Titulo: SOm
Tipo: View
FUncao: Carrega e executa sons quando chamados pelo programa principal.
OBS: Grande parte dessa biblioteca foi retirada do Lazy FOO's guide de SDL 2.0.
*/

#ifndef EA872som
#define EA872som

#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include "universalHeader.h"
#include <vector>

#define maxSom 100 



class Som
{
	public:
		static void iniciaPlayer();
		static Som* adiciona(std::string path, unsigned int);
		static void toca(Som*);
		static void toca(unsigned int );

		static Som* listaSom[maxSom];
		
		Mix_Chunk *arquivoAudio;
		unsigned int index;
		Som(std::string path, unsigned int);
		~Som();
		
};


#endif
