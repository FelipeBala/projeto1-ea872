/*
*Modulo: Controlador
*Tipo: CONTROLER (ah va)
*Funcao: Manipula as entidades, no caso o heroi e os monstros. Implementa funcoes que mudam o estado desses.
No caso do heroi, efetiva os comandos dados pelo usuario e efeitos do ambiente. No caso dos monstros,
implementa o comportamento de perseguir e atacar o Heroi.
*/

#ifndef EA872controle
#define EA872controle

#include "universalHeader.h"
#include <vector>
#include <chrono>
#include "imagem.h"
#include "entidade.h"
#include "entidades.h"
#include "mapa.h"
#include "colisor.h"
#include "heroi.h"
#include "monstro.h"
#include <string>


enum Direcao
{CIMA, BAIXO, ESQUERDA, DIREITA};

class Controle
{
	public:
		//Metodos:
		//Movimenta heroi, se nao gerar colisao (responde acao do usuario)
		static void move(Direcao dir, Entidade*);
		//Realiza um ataque com o heroi, e efetiva dano e experiencia recebida
		static void ataca(Heroi *heroi);
		//Realiza um ataque no heroi, possivelmente causando a morte
		static void recebeAtaque(int valorAtaque, Heroi *heroi);
		//Realiza um ataque em um monstro, possivelmente causando a morte, sendo o retorno a exp obtida
		static int recebeAtaque(int valorAtaque, Monstro *monstro);
		//Spawna monstro no jogo
		static void criaMonstro();
		//Movimenta monstros em direcao ao heroi
		static void movimentaMonstros();
		//Restaura entidades que fizeram ataques ao modo padrao
		static void restauraEstados();
		//MOnstros tem a chance de verificar se estao perto do heroi e realizar ataque
		static void monstrosAtacam();
		//Aplica condicoes de morte ao heroi
		static void heroiMorre(Heroi *heroi);
		//ELimina monstro das entidades existente
		static void monstroMorre(Monstro *monstro);
		//Aplica aumento de level no heroi
		static void sobeLevelHeroi(Heroi *heroi);
		//Cria novo heroi
		static Heroi* criaHeroi(std::string path);

		//Variaveis usadas para marcar tempo entre certas acoes 
		static uint64_t timerCriaMonstros;
		static uint64_t timerMovimentoMonstros;
		static uint64_t timerAtaqueMonstros;
};


#endif
