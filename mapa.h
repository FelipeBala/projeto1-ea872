/*
Titulo: Mapa
Tipo:MOdel (apesar de ser muito usado pelo view)
FUncao: Representa simplesmente o mapa do jogo, guarda sua imagem e mantem sua posicao em relacao a tela.
*/

#ifndef EA872mapa
#define EA872mapa
#include "universalHeader.h"
#include "imagem.h"

class Mapa
{
	public:
		int larguraTotal;
		int alturaTotal;
		Imagem imagem;
		bool carregarImagem(std::string path);
		void renderiza(SDL_Rect playerViewPort);
};


#endif