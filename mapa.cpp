/*
Titulo: Mapa
Tipo:MOdel (apesar de ser muito usado pelo view)
FUncao: Representa simplesmente o mapa do jogo, guarda sua imagem e mantem sua posicao em relacao a tela.
*/

#include "mapa.h"
#include "imagem.h"

bool Mapa::carregarImagem(std::string path){
	bool sucesso = this->imagem.loadFromFile( path , false);
	if(sucesso){
		this->larguraTotal = this->imagem.getWidth();
		this->alturaTotal = this->imagem.getHeight();
	}

	return sucesso;
}

void Mapa::renderiza(SDL_Rect playerViewPort){
	this->imagem.viewPortRender(playerViewPort);
}