/*
*Modulo: Monstro
*Tipo: Model
*Funcao: Implementa caracteristicas do monstro que permitem que ele atue (ande, entre em combate, etc)
*/

#ifndef EA872monstro
#define EA872monstro

#include "entidade.h"


class Monstro: public Entidade{
	public:
		//Vida atual do monstro
		int vida;
		//Level (modifica poder de combate)
		int level;
		//Xp recebido se matar o monstro
		int recompensaExp;
		//COnstrutor
		Monstro(int px, int py, std::string path, int level);
		//MOdificadores dependentes do level
		int modificadorAtaque();
		int modificadorDefesa();
		int vidaMaxima();
};

#endif
