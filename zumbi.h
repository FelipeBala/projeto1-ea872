#ifndef EA872zumbi
#define EA872zumbi

#include "universalHeader.h"
#include "entidade.h"
#include "monstro.h"



class Zumbi : public Monstro
{
	public:
		Zumbi(int , int, int);
		Zumbi(int , int);

		void anda();

};


#endif
