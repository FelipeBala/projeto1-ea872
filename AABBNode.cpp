/*
*Modulo: AABBNode
*Tipo: Control
*Funcao: Usado como unidade fundamental da estrutura de dados AABBTree, suada na deteccao de colisoes.
*/

#include "AABBNode.h"
#include <stddef.h>
#include <algorithm>

AABBNode::AABBNode(void)
{
	this->parent = NULL;

	this->entity = NULL;

	this->height = 0;
	this->left = right = NULL;
}

AABBNode::AABBNode(void *_entity, int _minX, int _maxX,  int _minY,  int _maxY)
{
	this->entity = _entity;
	
	this->minX = _minX;
	this->maxX = _maxX;
	this->minY = _minY;
	this->maxY = _maxY;

	this->height = 0;

	this->left = this->right = this->parent = NULL;
}

AABBNode::AABBNode(AABBNode *leftChild, AABBNode *rightChild){
	this->minX = std::min(leftChild->minX, rightChild->minX);
	this->maxX = std::max(leftChild->maxX, rightChild->maxX);
	this->minY = std::min(leftChild->minY, rightChild->minY);
	this->maxY = std::max(leftChild->maxY, rightChild->maxY);

	this->left = leftChild;
	this->right = rightChild;

	leftChild->parent = rightChild->parent = this;

	this->height = std::max(leftChild->height, rightChild->height) + 1;

	this->entity = NULL;

	this->parent = NULL;
}

bool AABBNode::isLeaf(){
	return !(this->entity == NULL);
}

void AABBNode::adjustBox(AABBNode *leftChild, AABBNode *rightChild){
	this->minX = std::min(leftChild->minX, rightChild->minX);
	this->maxX = std::max(leftChild->maxX, rightChild->maxX);
	this->minY = std::min(leftChild->minY, rightChild->minY);
	this->maxY = std::max(leftChild->maxY, rightChild->maxY);
}

bool AABBNode::isRoot(){
	return (this->parent == NULL);
}

int AABBNode::increasedArea(AABBNode *inserted){
	int current_area = (this->maxX - this->minX)*(this->maxY - this->minY);
	int newX = std::max(this->maxX, inserted->maxX) - std::min(this->minX, inserted->minX); 
	int newY = std::max(this->maxY, inserted->maxY) - std::min(this->minY, inserted->minY);
	return newX*newY - current_area;
}

void AABBNode::mergeBoxes(AABBNode *inserted){
	this->minX = std::min(this->minX, inserted->minX);
	this->maxX = std::max(this->maxX, inserted->maxX);
	this->minY = std::min(this->minY, inserted->minY);
	this->maxY = std::max(this->maxY, inserted->maxY);
}

bool AABBNode::collide(AABBNode *other){
	return (this->maxX > other->minX && this->minX < other->maxX &&
		this->maxY > other->minY && this->minY < other->maxY );
}

int AABBNode::balance(){
	if(this->isLeaf())
		return 0;

	return this->right->height - this->left->height; 
}
