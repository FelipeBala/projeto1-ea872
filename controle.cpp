/*
*Modulo: Controlador
*Tipo: CONTROLER (ah va)
*Funcao: Manipula as entidades, no caso o heroi e os monstros. Implementa funcoes que mudam o estado desses.
No caso do heroi, efetiva os comandos dados pelo usuario e efeitos do ambiente. No caso dos monstros,
implementa o comportamento de perseguir e atacar o Heroi.
*/

#include "controle.h"
#include "zumbi.h"
#include <set>
#include <string>

//Valores de regulacao do jogo
#define TEMPO_DURACAO_ACAO_US 300000
#define TEMPO_CRIACAO_MONSTRO_MS 10000
#define TEMPO_MOVIMENTO_MONSTROS_MS 500
#define TEMPO_ENTRE_ATAQUES_MMONSTROS_MS 1000
#define POSICAO_RENACER_X 100
#define POSICAO_RENACER_Y 100
#define MAX_ENTIDADES 20
#define MAX_TENTATIVAS_SPAWN 10
#define RAND_X_POS 100 + rand()%1300
#define RAND_Y_POS 100 + rand()%800 
#define INF 2000000000 

//Temporizadores usados para colocar intervalos minimos entre acoes do controlador
using namespace std::chrono;
uint64_t Controle::timerCriaMonstros = duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count();
uint64_t Controle::timerMovimentoMonstros = duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count();
uint64_t Controle::timerAtaqueMonstros = duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count();

/*
Tenta mover o personagem, possivelmente falhando se o movimento gerar uma colisao.
*/
void Controle::move(Direcao dir, Entidade *enti){
	//Tipo do deslocamento
	switch(dir){
		case CIMA:
			enti->y -= enti->speed;
			break;
		case BAIXO:
			enti->y += enti->speed;
			break;
		case ESQUERDA:
			enti->x -= enti->speed;
			break;
		case DIREITA:
			enti->x += enti->speed;
	}
	
	//Muda acao do usuario para andando
	enti->acao = ANDANDO;
	//Muda, se necessario, a orientacao atual do personagem
	if(dir == ESQUERDA)
		enti->orientacao = ESQ;
	else if(dir == DIREITA)
		enti->orientacao = DIR;

	//Detecta se ha colisoes com outras entidades
	std::vector<void*> colisoes = Colisor::checaColisao(enti);

	//Detecta se o usuario saiu do mapa ou colidiu com outra entidade
	if(colisoes.size() <= 1 && enti->x < 1540 && enti->x > -120 && enti->y > -130 && enti->y < 945){
		//Efetiva a mudanca de posicao no gerenciador de colisoes
		Colisor::remove(enti);
		Colisor::insere(enti);
	}


	//Se houve colisao/saida do mapa, reverte a mudanca
	else{
		switch(dir){
			case CIMA:
				enti->y += enti->speed;
				break;
			case BAIXO:
				enti->y -= enti->speed;
				break;
			case ESQUERDA:
				enti->x += enti->speed;
				break;
			case DIREITA:
				enti->x -= enti->speed;
		}
	}
}

/*
Faz com que o usuario gere um ataque na direcao para qual ele esta orientado no momento
*/
void Controle::ataca(Heroi *heroi){
	//Se a entidade ja estiver atacando, nao pode atacar denovo ateh voltar a acao de andar
	if(heroi->acao == ATACANDO)
		return;
	//Muda a atual para atacando e marca quando ela comecou
	heroi->acao =ATACANDO;
	gettimeofday(&(heroi->momentoUltimaAcao), NULL);

	//Gera uma regiao de ataque na orientacao atual do Heroi
	AABBNode regiaoAtaque;

	if(heroi->orientacao == DIR)
		regiaoAtaque = AABBNode(NULL, heroi->x + heroi->getWidth(), heroi->x + 2*(heroi->getWidth()),
			heroi->y , heroi->y + heroi->getHeight());
	else
		regiaoAtaque = AABBNode(NULL, heroi->x - heroi->getWidth(), heroi->x,
			heroi->y , heroi->y + heroi->getHeight());


	//Verifica, com o gerador de colisao, quais entidades foram atingidas
	std::vector<void*> atingidos = Colisor::checaColisao(&regiaoAtaque);
	
	//Para as entidades atingidas que sao monstros, da o dano nelas e recebe a experiencia
	for(std::vector<void*>::iterator it = atingidos.begin(); it != atingidos.end(); it++){
		if(((Entidade*)*it)->tipo == MONSTRO){
			int ataque = heroi->modificadorAtaque() + (rand()%20 + 1);
			int ganhoExp = recebeAtaque(ataque, ((Monstro*)(*it)));
			heroi->exp += ganhoExp;
		}
	}

	//Sobe de level, se for o caso
	if(heroi->exp >= heroi->expProximoLevel()){
		sobeLevelHeroi(heroi);
	} 
}

/*
Todos os monstros no mapa verificam se ha um heroi por perto, e se for o caso dao um ataque nesse
*/
void Controle::monstrosAtacam(){
	//Se nao deu tempo suficiente entre ataques, nao faz nada
	if(duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count() - timerAtaqueMonstros < TEMPO_ENTRE_ATAQUES_MMONSTROS_MS){
		return;
	}
	timerAtaqueMonstros = duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count();
	for(std::set<Entidade*>::iterator it = Entidades::listaEntidades.begin(); it != Entidades::listaEntidades.end(); it++){
		//Se a entidade ja estiver atacando, nao pode atacar denovo ateh voltar a acao de andar
		//Tambem pula entidade se nao for um monstro
		if((*it)->acao == ATACANDO || (*it)->tipo != MONSTRO)
			continue;
		//Muda a atual para atacando e marca quando ela comecou
		gettimeofday(&((*it)->momentoUltimaAcao), NULL);

		//Regioes de ataques a direita e a esquerda
		AABBNode regiaoPossivelAtaque = AABBNode(NULL, (*it)->x + (*it)->getWidth(), (*it)->x + 2*((*it)->getWidth()),(*it)->y , (*it)->y + (*it)->getHeight());
	
		//Procura pelo heroi na regiao direita
		bool heroiEncontrado = false;
		std::vector<void*> atingidos = Colisor::checaColisao(&regiaoPossivelAtaque);
		//Procura heroi entre as entidades encontradas
		for(std::vector<void*>::iterator it2 = atingidos.begin(); it2 != atingidos.end(); it2++){
			//Se encontra heroi executa ataque e vira o monstro para a direita
			if(((Entidade*)*it2)->tipo == HEROI){
				heroiEncontrado = true;
				//Dano do ataque depende de um modificador do monstro e mais um fator aleatorio (d20)
				int ataque = ((Monstro*)(*it))->modificadorAtaque() + (rand()%20 + 1);
				recebeAtaque(ataque, ((Heroi*)*it2));
				(*it)->orientacao = DIR;
			}
		}

		//Se eroi ainda nao foi encontrado, procura por ele na regiao esquerda
		if(heroiEncontrado == false){
			//Gera regiao esquerda e recebe entidades nela
			regiaoPossivelAtaque = AABBNode(NULL, (*it)->x - (*it)->getWidth(), (*it)->x, (*it)->y , (*it)->y + (*it)->getHeight());
			std::vector<void*> atingidos = Colisor::checaColisao(&regiaoPossivelAtaque);
			//Procura heroi entre as entidades encontradas
			for(std::vector<void*>::iterator it2 = atingidos.begin(); it2 != atingidos.end(); it2++){
				//Se encontra heroi executa ataque e vira o monstro para a esquerda
				if(((Entidade*)*it2)->tipo == HEROI){
					heroiEncontrado = true;
					//Dano do ataque depende de um modificador do monstro e mais um fator aleatorio (d20)
					int ataque = (((Monstro*)*it))->modificadorAtaque() + (rand()%20 + 1);
					recebeAtaque(ataque, ((Heroi*)*it2));
					(*it)->orientacao = ESQ;
				}
			}
		}

		//Se houve ataque, marca que o monstro esta atacando
		if(heroiEncontrado){
			(*it)->acao = ATACANDO;
			gettimeofday(&((*it)->momentoUltimaAcao), NULL);
		}
	}
}


/*
Heroi passado recebe o dano, aplica sua defesa e possivelmente morre
*/
void Controle::recebeAtaque(int valorAtaque, Heroi *heroi){
	//Dano baseado na defesa do heroi em em um um fator aleatorio (d20)
	int dano = std::max(0, valorAtaque - heroi->modificadorDefesa() - (rand()%20+1));
	heroi->vida -= dano;
	if(heroi->vida <= 0)
		heroiMorre(heroi);
}

/*
Calcula o dano dado no mosntro, considerando um fator aleatorio (d20) e seu fator de defesa
Se o monstro morre, chama a funcao para efetivar a eliminacao
Retorna zero se o monstro nao morrer, e a experiencia obtida caso ele tenha morrido
*/
int Controle::recebeAtaque(int valorAtaque, Monstro *monstro){
	int dano = std::max(0, valorAtaque - monstro->modificadorDefesa() + (rand()%20+1));
	monstro->vida -= dano;
	if(monstro->vida <= 0){
		int recompensa = monstro->recompensaExp;
		monstroMorre(monstro);
		return recompensa;
	}

	return 0;
}

/*
Cria um zumbi em uma posicao aleatoria do mapa
*/
void Controle::criaMonstro()
{
	//Se ja houver o maximo de entidades, nao cria uma nova
	if(Entidades::listaEntidades.size() >= MAX_ENTIDADES)
		return;
	if(duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count() - timerCriaMonstros > TEMPO_CRIACAO_MONSTRO_MS){
		timerCriaMonstros = duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count();		
		srand (time(NULL));

		Entidade *novaEntidade;

		//Com 90% de chance cria um zumbi level 1 no mapa
		if(rand()%10 != 0)
			novaEntidade = new Zumbi(100+rand()%1500,100+rand()%900);
		//Com 10%, cria um monstro level 10
		else
			novaEntidade = new Monstro(100+rand()%1500, 100+rand()%900,"imagens/balrog", 10);

		//Tenta no maximo um certo numero de vezes colocar ele no mapa sem colisao
		int contador = MAX_TENTATIVAS_SPAWN;

		while(contador--){
			//Se nao ha colisao, termina
			if(Colisor::checaColisao(novaEntidade).size() == 0)
				return;

			novaEntidade->x = RAND_X_POS;
			novaEntidade->y = RAND_Y_POS;
		}

		//Se nao conseguiu, deleta entidade
		Entidades::removeEntidade(novaEntidade);
	}

}


/*
Faz todos os monstros existentes se movimentarem na direcao do heroi, verificando colisoes
*/
void Controle::movimentaMonstros(){
	if(duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count() - timerMovimentoMonstros > TEMPO_MOVIMENTO_MONSTROS_MS){
		timerMovimentoMonstros = duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count();

		//Localiza herois
		std::vector<std::pair<int, int> > alvos;
		for(std::set<Entidade*>::iterator it = Entidades::listaEntidades.begin(); it != Entidades::listaEntidades.end(); it++)
			if((*it)->tipo == HEROI)
				alvos.push_back(std::make_pair((*it)->x, (*it)->y));

		//Se nao ha herois para seguir, os monstros nao se movem
		if(alvos.size() == 0)
			return;

		for(std::set<Entidade*>::iterator it = Entidades::listaEntidades.begin(); it != Entidades::listaEntidades.end(); it++){
			//Se for um monstro, tenta realisar movimento (pode ser empedido por colisoes)
			if( (*it)->tipo == MONSTRO){
				//Seleciona heroi mais proximo
				int x, y;
				int mn = INF;
				for(auto a: alvos){
					if(abs(a.first - (*it)->x) + abs(a.second - (*it)->y) <= mn){
						x = a.first;
						y = a.second;
						mn = abs(a.first - (*it)->x) + abs(a.second - (*it)->y);
					}
				}

				//Verifica movimento a ser realizado para perseguir jogador
				int deltaX, deltaY;
				if( ( (*it)->x - x ) > 0){
					deltaX = -(*it)->speed;
				}else{
					deltaX = (*it)->speed;
				}
				if( ( (*it)->y - y ) > 0){
					deltaY = -(*it)->speed;
				}else{
					deltaY = (*it)->speed;
				}

				//Realiza movimento
				(*it)->x += deltaX;
				(*it)->y += deltaY;

				//Remove a caixa anterior do controlador de colisao
				Colisor::remove(*it);
				
				//Verifica se havera colisao com a nova caixa
				if(Colisor::checaColisao(*it).size() != 0){
					//Caso exista colisao, reverte o movimento
					(*it)->x -= deltaX;
					(*it)->y -= deltaY;
				}

				//Insere nova caixa de colisao
				Colisor::insere(*it); 

				//Atualiza o sentido para qual o monstro esta virado
				if(deltaX > 0)
					(*it)->orientacao = DIR;
				else
					(*it)->orientacao = ESQ;

			}
		}
	}

}

/*
Verifica, para cada entidade que esteja executando uma acao (atacando), se o tempo que essa deveria durar ja acabou.
Em caso positivo, devolve a entidade para a acao padrao (andando).
*/
void Controle::restauraEstados(){
	//Verifica momento atual
	timeval momentoAtual, tempoTranscorrido;
	gettimeofday(&(momentoAtual), NULL);

	//Itera pelas entidades verificando quais estao agindo e possivelmente devem ser paradas
	for(std::set<Entidade*>::iterator it = Entidades::listaEntidades.begin(); it != Entidades::listaEntidades.end(); it++){
		if((*it)->acao != ANDANDO){
			timersub(&momentoAtual, &((*it)->momentoUltimaAcao), &tempoTranscorrido);
			if(tempoTranscorrido.tv_usec > TEMPO_DURACAO_ACAO_US)
				(*it)->acao = ANDANDO;
		}
	}
}

/*
Faz com que o heroi morra, levando-o devolta a posicao de respawn.
Enche sua vida, mas retira todo sua experiencia.
*/
void Controle::heroiMorre(Heroi *heroi){
	Colisor::remove(heroi);
	heroi->x = POSICAO_RENACER_X;
	heroi->y = POSICAO_RENACER_Y;
	while(Colisor::checaColisao(heroi).size() != 0){
		heroi->x = RAND_X_POS;
		heroi->y = RAND_Y_POS;
	}
	Colisor::insere(heroi);
	heroi->exp = 0;
	heroi->vida = heroi->vidaMaxima();
}

/*
Retira o monstro da lista de entidades
*/
void Controle::monstroMorre(Monstro *monstro){
	Entidades::removeEntidade(monstro);
}

/*
Incrementa o level do heroi, reinicia a barra de experiencia e recupera a vida do heroi
*/
void Controle::sobeLevelHeroi(Heroi *heroi){
	heroi->level++;
	heroi->exp = 0;
	heroi->vida = heroi->vidaMaxima();
}

/*
Cria um novo heroi, carregando a imagem a partir do caminho passado
*/
Heroi* Controle::criaHeroi(std::string path){
	Heroi *player = new Heroi(100, 100, "imagens/character");
	while(Colisor::checaColisao(player).size() != 0){
		player->x = RAND_X_POS;
		player->y = RAND_Y_POS;
	}

	return player;
}
