/*
Titulo: Entidades
Tipo:Model
FUncao: Tipo usado para manter o conjunto de todas as entidades existentes no mapa e iterar por elas quano necessario.
*/

#include "entidades.h"
#include "colisor.h"

std::set<Entidade*> Entidades::listaEntidades;

void Entidades::addEntidade(Entidade* e){
	Colisor::insere(e);
	Entidades::listaEntidades.insert(e);
}

bool Entidades::removeEntidade(Entidade* e){
	bool sucess = Colisor::remove(e);
	sucess &= (listaEntidades.find(e) != listaEntidades.end());
	if(sucess){ 
		Entidades::listaEntidades.erase(e);
		delete e;
	}

	return sucess;
}

bool Entidades::removeEntidadeSemDeletar(Entidade* e){
	bool sucess = Colisor::remove(e);
	sucess &= (listaEntidades.find(e) != listaEntidades.end());
	if(sucess){ 
		Entidades::listaEntidades.erase(e);
	}

	return sucess;
}

void Entidades::esvaziaEntidades(){
	while(!Entidades::listaEntidades.empty()){
		Entidades::removeEntidade(*(Entidades::listaEntidades.begin()));
	}
}

void Entidades::addMap(std::map<int, Entidade> mapaIdEntidade){
  for (std::map<int, Entidade>::iterator it=mapaIdEntidade.begin(); it!=mapaIdEntidade.end(); ++it){
      Entidades::addEntidade(&(it->second));
  }
}
