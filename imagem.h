/*
Titulo: Imagem
Tipo:View
FUncao: Tipo usado carregar e manipular as imagens exibidas, tanto de entidades quanto do mapa do jogo.
OBS: Grande parte dessa biblioteca foi retirada do Lazy FOO's guide de SDL 2.0.
*/

#ifndef EA872imagem
#define EA872imagem

#include "universalHeader.h"

class Imagem
{

	public:
		Imagem();
		void free();
		bool loadFromFile( std::string path , bool useColorKeying = true);
		void render( int x, int y );
		int getWidth();
		int getHeight();
		void viewPortRender(SDL_Rect playerViewPort);

	private:
		//The actual hardware texture
		SDL_Texture* mTexture;
		//Image dimensions
		int mWidth;
		int mHeight;


};


#endif
