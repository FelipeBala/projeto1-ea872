/*
Titulo: Entidade
Tipo:Model
FUncao: Tipo generico para elementos do jogo que se movem e tem acoes, como monstros e herois.
*/

#include "entidade.h"
#include "entidades.h"
#include "colisor.h"
#include<stdio.h>

long Entidade::iid = 0;

long Entidade::getId(){
	Entidade::iid++;
	return Entidade::iid-1;
}

Entidade::Entidade(){
	this->x = 0;
	this->y = 0;
	this->id = Entidade::getId();
	Entidades::addEntidade(this);
}

Entidade::Entidade(long int pid){
	this->id = pid;
	this->x = 0;
	this->y = 0;
	Entidades::addEntidade(this);
}

Entidade::Entidade(int px, int py){
	this->id = Entidade::getId();
	this->x = px;
	this->y = py;
	Entidades::addEntidade(this);
}

Entidade::Entidade(std::string path){
	this->id = Entidade::getId();
	this->x = 0;
	this->y = 0;
	this->carregarImagem(path);
	this->imagePath = path;
	Entidades::addEntidade(this);
}

Entidade::Entidade(int px, int py, std::string path){
	this->id = Entidade::getId();
	this->x = px;
	this->y = py;
	this->carregarImagem(path);
	this->imagePath = path;
	Entidades::addEntidade(this);
}

bool Entidade::carregarImagem(std::string path){
	bool sucess = this->imagem[ESQ][ANDANDO].loadFromFile( path + "Esq.png");
	sucess &= this->imagem[DIR][ANDANDO].loadFromFile( path + "Dir.png");
	sucess = this->imagem[ESQ][ATACANDO].loadFromFile( path + "AtackEsq.png");
	sucess &= this->imagem[DIR][ATACANDO].loadFromFile( path + "AtackDir.png");
	return sucess;
}

void Entidade::renderiza(int xRef, int yRef){
	this->imagem[orientacao][acao].render(this->x + xRef, this->y + yRef);
}

int Entidade::getWidth(){
	return this->imagem[orientacao][acao].getWidth();
}

int Entidade::getHeight(){
	return this->imagem[orientacao][acao].getHeight();
}

int Entidade::getXCenter(){
	return (this->x + this->getHeight())/2;
}

int Entidade::getYCenter(){
	return (this->y + this->getWidth())/2;
}


Entidade::~Entidade(){
	Entidades::removeEntidade(this);
}


