/*
Titulo: SOm
Tipo: View
FUncao: Carrega e executa sons quando chamados pelo programa principal.
OBS: Grande parte dessa biblioteca foi retirada do Lazy FOO's guide de SDL 2.0.
*/


#include "som.h"
Som* Som::listaSom[100];

Som::Som(std::string path, unsigned int index){
	if(index >= 0 && index < maxSom){
		//Load sound effects
		this->index = index;
		this->arquivoAudio = NULL;
		this->arquivoAudio = Mix_LoadWAV( path.c_str() );
		if( this->arquivoAudio == NULL )
		{
			printf( "Failed to load scratch sound effect! SDL_mixer Error: %s\n", Mix_GetError() );
		}else{
			Som::listaSom[index] = this;
		}
	}
	
}

Som::~Som(){
	Mix_FreeChunk( this->arquivoAudio );
	Som::listaSom[this->index] = NULL;
}


void Som::iniciaPlayer(){
	//if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
	if( Mix_OpenAudio( 22050, AUDIO_S8, 2, 4096 ) < 0 )
	{
		printf( "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError() );
	}
}

Som* Som::adiciona(std::string path, unsigned int index){
	Som *a = new Som(path,index);
	return a;
}


void Som::toca(Som *audio){
	if(audio->arquivoAudio != NULL){
		Mix_PlayChannel( -1, audio->arquivoAudio, 0 );
	}else{
		printf("Falha ao tocar o som.\n");
	}
}

void Som::toca(unsigned int index){
	Som *audio = Som::listaSom[index];
	if(audio != NULL){
		Som::toca(audio);
	}else{
		printf("Falha - acesso a index de som inexistente.\n");
	}
}
