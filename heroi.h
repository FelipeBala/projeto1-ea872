/*
*Modulo: Heroi
*Tipo: Model
*Funcao: Implementa caracteristicas do heroi que permitem que ele atue (ande, entre em combate, etc)
*/

#ifndef EA872heroi
#define EA872heroi

#include "entidades.h"
#include "heroi.h"
#include <string>
#include <vector>

class Heroi: public Entidade{
	public:
		//Vida, level e experiencia atuais do heroi
		int vida;
		int level;
		int exp;
		//COnstrutor
		Heroi(int px, int py, std::string path);
		//Modificadores dependentes de level do heroi
		int modificadorAtaque();
		int modificadorDefesa();
		int vidaMaxima();
		//Exp antes de subir para o proximo level, dependente do level atual
		int expProximoLevel();

		static std::vector<Heroi> ListaJogadores; 
};

#endif
