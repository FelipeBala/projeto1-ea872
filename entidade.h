/*
Titulo: Entidade
Tipo:Model
FUncao: Tipo generico para elementos do jogo que se movem e tem acoes, como monstros e herois.
*/

#ifndef EA872entidade
#define EA872entidade

#include "universalHeader.h"
#include <vector>
#include "imagem.h"
#include "entidade.h"
#include "mapa.h"


enum orientacaoHorizontal
{ESQ, DIR};

enum tipoAcao
{ANDANDO, ATACANDO};

enum tipoEntidade
{HEROI, MONSTRO, OBJETO};

class Entidade
{
	public:
		Entidade();
		Entidade(long int pid);
		Entidade(int px, int py);
		Entidade(std::string path);
		Entidade(int px, int py,std::string path);
		bool carregarImagem(std::string path);
		void renderiza(int xRef, int yRef);

		int getWidth();
		int getHeight();
		int getXCenter();
		int getYCenter();

		static long iid;
		static long getId();

		long id;
		int x,y;
		int speed = 20;
		bool rederizavel=true;
		tipoEntidade tipo;
		Imagem imagem[2][2];
		orientacaoHorizontal orientacao = DIR;
		tipoAcao acao = ANDANDO;
		timeval momentoUltimaAcao;
		std::string imagePath;
		~Entidade();

};


#endif
