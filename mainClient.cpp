#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>


#include "universalHeader.h"
#include "render.h"
#include "imagem.h"
#include "entidades.h"
#include "som.h"
#include "mapa.h"
#include "controle.h"
#include "heroi.h"
#include "monstro.h"
#include "zumbi.h"
#include "servidor.h"

#include "jsonManager.h"

#include <stropts.h>
#include <poll.h>

#define TempoEspera 0

int criaConexaoServidor(char *targetIp);
bool conexoesRede(int socket_fd, char *input_buffer);
void atualizaEntidades(Heroi &jogador, char *input_buffer);



int main(int argc, char **argv) {
   int socket_fd;
   char targetIp[16], input_buffer[buffer];

  
   //Verifica se o usuario passou o IP.	
   if (argc != 2){
     printf("Entre com o IP alvo:\n");
     scanf("%s", targetIp);	
   }else{
   strcpy(targetIp, argv[1]);

  }


   Render::init(640, 480);
   Heroi *jogador = new Heroi(100,100,"imagens/character");
   Entidades::removeEntidadeSemDeletar(jogador);

   //Mapa do jogo
   Mapa mapaGeral;
   mapaGeral.carregarImagem("imagens/mapa.png");

   //Carrega sons do jogo
   Som::iniciaPlayer();
   Som a("som/scratch.wav",1);
   Som::adiciona("som/high.wav",2);
   Som::adiciona("som/step.wav",3);

   // Configura as variaveis de rede.
   if ( (socket_fd =criaConexaoServidor(targetIp))  < 0) {return 0;}




   //Loop principal
   while(!Render::quit){

      Render::HandleEvent();
      Render::renderWorld(jogador, mapaGeral);
      if(conexoesRede(socket_fd, input_buffer)){
		    atualizaEntidades(*jogador, input_buffer);
      }
      //new Zumbi(200,200);
   }




   close(socket_fd);
   //Fecha janela
   Render::DestroyWindow();
   return 0;
}



int criaConexaoServidor(char *targetIp){

   struct sockaddr_in *target = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));
   int socket_fd;

   socket_fd = socket(AF_INET, SOCK_STREAM, 0);
   printf("Cliente - Socket criado\n");

   target->sin_family = AF_INET;
   target->sin_port = htons(3001);
   inet_aton(targetIp, &(target->sin_addr));
   printf("Cliente - Tentando conectar\n");
   if (connect(socket_fd, (struct sockaddr*)target, sizeof(*target)) != 0) {
     printf("Cliente - Problemas na conexao\n");
     return -1;
   }
   printf("Cliente - Conectei ao servidor\n");
   return socket_fd;

}

bool conexoesRede(int socket_fd, char *input_buffer){

   int rv,n;
   struct pollfd ufds;

   //Cria regras do poll de I/O.
      ufds.fd = socket_fd;
      ufds.events = POLLIN | POLLPRI;  

      std::string character(" \0"), comando("");

      character[0] = Render::tecla;	
      if(character[0] != ' '){
	JsonManager::criaJsonComando(character, comando);
        send(socket_fd, comando.c_str(), comando.length(), 0);
        //printf("Cliente - Enviei mensagem: %s\n", comando.c_str());
      }

      rv = poll(&ufds, 1, TempoEspera);

      if (rv == -1) {
           // Ocorreu um erro no poll
	   perror("poll");
	   return false;
      }

      if (rv == 0) {
	   //printf("Timeout !  Sem dados apos espera de %f segundos.\n",TempoEspera);
	   return false;
      }

      if(rv >= 1){

      	    if (ufds.revents & POLLIN || ufds.revents & POLLPRI ) {
		 /* Recebendo resposta */
   		 if ( (n=recv(socket_fd, input_buffer, buffer, 0) ) > 0){
		     input_buffer[n]=0;
   		     //printf("Cliente - resposta do servidor %d:\n%s\n",n, input_buffer);
		     return true;
		 }
	   
            } 
      }

return false;

}

void atualizaEntidades(Heroi &jogador, char *input_buffer){
	const std::string json(input_buffer);
	if(JsonManager::leJsonMundo(jogador, json)){
    return;
	}else{
		printf("Json Erro: %s\n", input_buffer);
	}
return;
}






