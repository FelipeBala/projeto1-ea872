/*
*Modulo: Monstro
*Tipo: Model
*Funcao: Implementa caracteristicas do monstro que permitem que ele atue (ande, entre em combate, etc)
*/

#include "universalHeader.h"
#include "monstro.h"
#include "heroi.h"
#include "entidades.h"
#include "colisor.h"
#include "controle.h"

#define VELOCIDADE_MONSTRO 3


//Cria monstro em posicao (px py), carrega imagem de path, e define o level do monstro
Monstro::Monstro(int px, int py, std::string path, int level)
	:Entidade(px, py, path){
		this->level = level;
		this->recompensaExp = level;
		this->vida = this->vidaMaxima();
		this->tipo = MONSTRO;
		this->speed = VELOCIDADE_MONSTRO;
	}


//Retorna vida maxima do monstro, dependendo do level desse
int Monstro::vidaMaxima(){
	return 10 + 10*this->level;
}

//Retorna modificador de ataque do monstro, dependente do level
int Monstro::modificadorAtaque(){
	return 2*level;
}

//Retorna modificador de defesa do monstro, dependente do level
int Monstro::modificadorDefesa(){
	return level;
}



